#!/usr/bin/env bash
green=$(tput setf 2)
toend=$(tput hpa $(tput cols))$(tput cub 6)

echo "Мы начинаем старт докера для тестового проекта"
echo -n "Развернуть базу данных? (y/n): "
read new_db
echo 'Запускаем сборку докера!'
docker-compose up -d || exit
echo -en '\n'
echo -n "Докер успешно собрался! ${green}${toend}[OK]"
echo -en '\n'
echo 'Теперь нам необходимо собрать композер.'
docker-compose exec -T "php" sh -c "cd /var/www/symfony && php composer.phar install -q"

case "$new_db" in
    y|Y)
    docker-compose exec -T "php" sh -c "cd /var/www/symfony && php bin/console doctrine:database:create"
    docker-compose exec -T "php" sh -c "cd /var/www/symfony && php bin/console doctrine:schema:create"
    ;;
    *) echo "Хорошо, обойдемся без БД! =)"
    ;;
esac

echo -en '\n'
echo 'Теперь почистим кэш!'
docker-compose exec -T "php" sh -c "cd /var/www/symfony && php bin/console cache:clear"
docker-compose exec -T "php" sh -c "cd /var/www/symfony && chown -R www-data:www-data vendor"
docker-compose exec -T "php" sh -c "cd /var/www/symfony && chown -R www-data:www-data var"
echo -en '\n'
echo -n "Кэш успешно очищен! ${green}${toend}[OK]"