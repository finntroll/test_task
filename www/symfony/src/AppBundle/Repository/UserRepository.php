<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param string $alias
     * @return QueryBuilder
     */
    public function prepareQueryBuilder($alias): QueryBuilder
    {
        $qb = $this->createQueryBuilder($alias);

        return $qb;
    }
}
