<?

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\User\UserForm;
use AppBundle\Manager\UserManager;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use LogicException;
use AppBundle\Repository\UserRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends ApiController
{
    /**
     * @Route("/{id}", name="/", requirements={"id"="\d+"}, methods={"GET"})
     * @param User|null $user
     *
     * @return JsonResponse
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws ServiceNotFoundException
     * @throws LogicException
     * @throws NonUniqueResultException
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK",
     *     @SWG\Schema(@Model(type=User::class))
     * )
     *
     * @SWG\Response(
     *      response=404,
     *      description="Not found",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="status",
     *              type="integer",
     *              example=404
     *          ),
     *          @SWG\Property(
     *              property="message",
     *              type="string",
     *              example="User not found"
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="user")
     */
    public function getUserInfoAction(?User $user): JsonResponse
    {
        if ($user === null) {
            throw new HttpException(404, 'User not found');
        }

        return $this->renderJson($user);
    }

    /**
     * @Route(
     *      name="user_list",
     *      methods={"GET"}
     *     )
     *
     * @Nelmio\Operation(
     *     summary="Getting user list",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          in="query",
     *          name="page",
     *          type="integer",
     *          description="Page number",
     *          default="1"
     *     ),
     *     @SWG\Parameter(
     *          in="query",
     *          name="count",
     *          type="integer",
     *          description="Elements count on one page",
     *          default="20"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK",
     *          headers={
     *                      @SWG\Header(header="X-Pagination-Page-Count", type="integer", description="Total page count")
     *                  },
     *          @SWG\Schema(@Model(type=User::class))
     *          )
     *     ),
     * )
     *
     * @param Request      $request
     *
     * @return JsonResponse
     *
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws NotFoundHttpException
     */
    public function getUserListAction(Request $request): JsonResponse
    {
        $page    = $request->query->getInt('page', self::FIRST_PAGE_NUMBER);
        $perPage = $request->query->getInt('count', self::PER_PAGE_COUNT);

        /**
         * @var UserRepository $repository
         */
        $repository  = $this->getDoctrine()->getRepository('AppBundle:User');

        /** @var SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $repository->prepareQueryBuilder('user'),
            $page,
            $perPage
        );

        return $this->renderJson(
            $pagination->getItems(),
            JsonResponse::HTTP_OK,
            null,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }

    /**
     * @Route(
     *      name="user_create",
     *      methods={"PUT"}
     *     )
     *
     * @Nelmio\Operation(
     *     summary="Creating of new user",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="First name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Last name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="middle_name",
     *         in="formData",
     *         description="Middle name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Registered user email",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[year]",
     *          in="formData",
     *          description="User's birth year",
     *          type="string",
     *          format="date-time",
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[month]",
     *          in="formData",
     *          description="User's birth month",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[day]",
     *          in="formData",
     *          description="User's birth day",
     *          type="string"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK",
     *          @SWG\Schema(@Model(type=User::class))
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *      )
     * )
     *
     * @param Request       $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @throws AlreadySubmittedException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function newUserAction(Request $request, UserManager $userManager)
    {
        $user = $userManager->createUser();

        $form = $this->createForm(UserForm::class, $user);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            return $this->renderJson($user);
        }

        return $this->errorJson(
            (string)$form->getErrors(true),
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

    /**
     * @Route(
     *      "/{id}",
     *      name="user_update",
     *      requirements={"id"="\d+"},
     *      methods={"PATCH"}
     *     )
     *
     * @Nelmio\Operation(
     *     summary="Updating of user",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="First name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Last name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="middle_name",
     *         in="formData",
     *         description="Middle name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Registered user email",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[year]",
     *          in="formData",
     *          description="User's birth year",
     *          type="string",
     *          format="date-time",
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[month]",
     *          in="formData",
     *          description="User's birth month",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[day]",
     *          in="formData",
     *          description="User's birth day",
     *          type="string"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK",
     *          @SWG\Schema(@Model(type=User::class))
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *      )
     * )
     *
     * @param User|null          $user
     * @param Request            $request
     *
     * @return JsonResponse
     *
     * @throws BadRequestHttpException
     * @throws \LogicException
     * @throws AlreadySubmittedException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws NotFoundHttpException
     */
    public function editUserAction(?User $user, Request $request): JsonResponse
    {
        if ($user === null) {
            throw new NotFoundHttpException('User not found.');
        }

        $form = $this->createForm(UserForm::class, $user);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->renderJson($user);
        }

        return $this->errorJson(
            (string)$form->getErrors(true),
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

    /**
     * @Route(
     *      "/{id}",
     *      name="user_delete",
     *      requirements={"id"="\d+"},
     *      methods={"DELETE"}
     *     )
     * @Nelmio\Operation(
     *     summary="Deleting of user",
     *     tags={"user"},
     *     @SWG\Parameter(
     *          in="path",
     *          name="id",
     *          required=true,
     *          type="integer",
     *          description="User ID"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="No Content"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *      )
     * )
     *
     * @param User|null $user
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws NotFoundHttpException
     * @throws \LogicException
     */
    public function deleteUserAction(?User $user, UserManager $userManager): JsonResponse
    {
        if ($user === null) {
            throw new NotFoundHttpException('User not found.');
        }

        try {
            $userManager->deleteUser($user);
        } catch (\Exception $exception) {
            return $this->errorJson($exception);
        }

        return new JsonResponse();
    }
}
