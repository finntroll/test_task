<?php

namespace AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 *
 * @package AppBundle\Controller
 */
abstract class ApiController extends Controller
{

    /**
     *
     */
    protected const PER_PAGE_COUNT = 20;

    /**
     *
     */
    protected const FIRST_PAGE_NUMBER = 1;

    /**
     *
     */
    protected const PAGINATION_COUNT_HEADER = 'X-Pagination-Page-Count';

    /**
     * @param string|Exception     $error
     * @param int                  $status
     * @param SerializationContext $context
     * @param array                $headers
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function errorJson($error, $status = Response::HTTP_INTERNAL_SERVER_ERROR, SerializationContext $context = null, array $headers = []): JsonResponse
    {
        $message = $error;

        if ($error instanceof Exception) {
            $message = $error->getMessage();
            $status  = $error->getCode();

            $logger = $this->get('logger');
            $logger->error($error->getTraceAsString());
        }

        return $this->renderJson(
            [
                'status'  => $status,
                'message' => $message
            ],
            $status,
            $context,
            $headers
        );
    }

    /**
     * @param mixed                $data
     *
     * @param int                  $status
     * @param SerializationContext $context
     * @param array                $headers
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function renderJson($data, $status = 200, SerializationContext $context = null, array $headers = []): JsonResponse
    {
        $serializer = $this->container->get('jms_serializer');

        return new JsonResponse(
            $serializer->serialize($data, 'json', $context),
            $status,
            $headers,
            true
        );
    }

    /**
     * @return ArrayCollection
     */
    protected function getJsonBody(): ArrayCollection
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $content = $request->getContent();

        return new ArrayCollection((array)json_decode($content, true));
    }
}