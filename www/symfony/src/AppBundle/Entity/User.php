<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 * @UniqueEntity("email")
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", length=50, nullable=false)
     *
     * @Assert\Type("string")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your name cannot contain a number"
     * )
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="middle_name", length=50, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your middle name cannot contain a number"
     * )
     */
    private $middleName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", length=50, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your last name cannot contain a number"
     * )
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(length=50, unique=true)
     *
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $email;

    /**
     * @var null|\DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     *
     * @Assert\Date()
     * @Serializer\Type("DateTime<'Y-m-d'>")
     */
    private $birthDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createTime;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName.
     *
     * @param string $middleName
     *
     * @return User
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName.
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|\DateTime $birthDate
     *
     * @return self
     */
    public function setBirthDate(?\DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $createTime
     *
     * @return self
     */
    public function setCreateTime(\DateTime $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateTime(): \DateTime
    {
        return $this->createTime;
    }
}
