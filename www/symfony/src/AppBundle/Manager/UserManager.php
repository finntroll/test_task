<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserManager
 *
 * @package AppBundle\Manager
 */
class UserManager
{

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * UserManager constructor.
     *
     * @param UserRepository $repository
     * @param EntityManagerInterface $em
     */
    public function __construct(UserRepository $repository, EntityManagerInterface $em)
    {
        $this->em = $em;

        $this->repository = $repository;
    }

    /**
     * @return User
     */
    public function createUser(): User
    {
        $user = new User();
        $user->setCreateTime(new \DateTime());

        return $user;
    }

    /**
     * @param User $user
     */
    public function deleteUser(User $user): void
    {
        $this->em->remove($user);
        $this->em->flush();
    }
}
