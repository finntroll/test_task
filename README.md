test_project
============

## Installation

Clone this repository on your local computer.

```shell
cd test_task/
bash start.sh
```

Your LAMP stack is now ready!! You can access it via `http://localhost`.

## Documentation

Интерактивная документация доступна по адресу `http://localhost/api/doc`.
